public class Board {
	private Square[][] ticTacToeBoard = new Square[3][3];
	//since the length for the array is 3, which is also the same for the nested arrays, we can use this on our for loop conditions to avoid having to waste a lot of time writing/copying the same long 'ticTacToeBoard.length'
	public int boardSize = ticTacToeBoard.length;
	
	public Board() {
		for(int i = 0; i < boardSize; i++) {
			for(int j = 0; j < boardSize; j++) {
				ticTacToeBoard[i][j] = Square.BLANK;
			}
		}
	}
	
	public String toString() {
		String boardToString = "  0 1 2";
		for(int i = 0; i < boardSize; i++) {
			boardToString += "\n" + i + " " + ticTacToeBoard[i][0] + " " + ticTacToeBoard[i][1] + " " + ticTacToeBoard[i][2];
		}
		return boardToString;
	}
	
	public void placeToken(int xloc, int yloc, Square playerToken) {
		if(xloc >= 0 && yloc >= 0) {
			if(ticTacToeBoard[xloc][yloc] == Square.BLANK) {
				ticTacToeBoard[xloc][yloc] = playerToken;
			} else {
				throw new IllegalArgumentException("That square slot is already taken!");
			}
		} else if(xloc < 0 || yloc < 0) {
			throw new IllegalArgumentException("The square slots input are invalid! (Negative values will not be accepted)");
		} else {
			throw new IllegalArgumentException("The square slots input are invalid! (These values are out of bounds! Please keep in mind " 
												+ "the board is 3x3!)");
		}
	}
	
	public static void main(String[] args) {
		Board game = new Board();
		
		game.ticTacToeBoard[2][0] = Square.X;
		game.ticTacToeBoard[1][1] = Square.X;
		game.ticTacToeBoard[1][2] = Square.X;
		
		System.out.println(game);
		System.out.println(game.checkIfWinDiag(Square.X));
	}
	
	private boolean checkIfWinH(Square playerToken) {
		for(int i = 0; i < boardSize; i++) {
			int tokenCounter = 0;
			for(int j = 0; j < boardSize; j++) {
				if(ticTacToeBoard[i][j] == playerToken) {
					tokenCounter++;
					
				}
			}
			if(tokenCounter == 3) {
				return true;
			}
		}
		return false;
	}
	
	private boolean checkIfWinV(Square playerToken) {	
		for(int i = 0; i < boardSize; i++) {
			int tokenCounter = 0;
			for(int j = 0; j < boardSize; j++) {
				if(ticTacToeBoard[j][i] == playerToken) {
					tokenCounter++;
					
				}
			}
			if(tokenCounter == 3) {
				return true;
			}
		}
		return false;
	}
	
	private boolean checkIfWinDiag(Square playerToken) {
		int tokenCounterLeft = 0;
		int tokenCounterRight = 0;
		for(int i = 0; i < boardSize; i++) {
			if(ticTacToeBoard[i][i] == playerToken) {
				tokenCounterLeft++;
			}
			if(ticTacToeBoard[i][2-i] == playerToken) {
				tokenCounterRight++;
			}
		}
		
		if(tokenCounterLeft == 3 || tokenCounterRight == 3) {
			return true;
		}
		return false;
	}
	
	public boolean checkIfWin(Square playerToken) {
		if(checkIfWinH(playerToken) || checkIfWinV(playerToken) || checkIfWinDiag(playerToken)) {
			return true;
		} else {
			return false;
		}
	}
	
	public boolean checkIfFull() {
		for(int i = 0; i < boardSize; i++) {
			int blankCounter = 0;
			for(int j = 0; j < boardSize; j++) {
				if(ticTacToeBoard[i][j] == Square.BLANK) {
					blankCounter++;
				}
			}
			if(blankCounter > 0) {
				return false;
			}
		}
		return true;
	}
}