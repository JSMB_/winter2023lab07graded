import java.util.Scanner;

public class ticTacToeGame {
	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		
		
		boolean multipleGame = true;
		
		int token1 = 0;
		int token2 = 0;
		
		int rounds = 1;
		int player1Wins = 0;
		int player2Wins = 0;
		
		System.out.println("Welcome to the Tic Tac Toe game!" + 
			"\nPlayer 1's Token: X" + 
			"\nPlayer 2's Token: O");
		while(multipleGame) {
			Board game = new Board();
			System.out.println("ROUND " + rounds);
			System.out.println(game);
			boolean continueGame = true;
			while(continueGame) {
				Square p1Token = Square.X;
				System.out.println("It's player 1's turn. Where will you place your token?");
				token1 = sc.nextInt();
				token2 = sc.nextInt();
				game.placeToken(token1, token2, p1Token);
				System.out.println(game);
				if(game.checkIfWin(p1Token)) {
					System.out.println("Player 1 wins!");
					player1Wins++;
					continueGame = false;
				} else if(game.checkIfFull()) {
					System.out.println("No one won! (tie)");
					continueGame = false;
				} else {
					Square p2Token = Square.O;
					System.out.println("It's player 2's turn. Where will you place your token?");
					token1 = sc.nextInt();
					token2 = sc.nextInt();
					game.placeToken(token1, token2, p2Token);
					System.out.println(game);
					
					if(game.checkIfWin(p2Token)) {
						System.out.println("Player 2 wins!");
						player2Wins++;
						continueGame = false;
					} else if(game.checkIfFull()) {
						System.out.println("No one won! (tie)");
						continueGame = false;
					}
				}
			}
			System.out.println("Player 1 wins: " + player1Wins + "\nPlayer 2 wins: " + player2Wins +
			"\nWould you like to play another round? (Type Y/N)");
			
			String startNewGame = sc.next();
			
			if(startNewGame.toUpperCase().equals("Y")) {
				rounds++;
			} else if(startNewGame.toUpperCase().equals("N")) {
				multipleGame = false;
			}
		}
		
		System.out.println("Total rounds played: " + rounds + 
		"\nTotal player 1 wins: " + player1Wins + 
		"\nTotal player 2 wins: " + player2Wins);
	}
}